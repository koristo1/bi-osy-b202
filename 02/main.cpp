#ifndef __PROGTEST__

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <cassert>
#include <cmath>
#include <iostream>

using namespace std;
#endif /* __PROGTEST__ */
;

const int SMALLEST_LEVEL = 4;
const int BIGGEST_LEVEL = 31;

const int SMALLEST_BLOCK_SIZE = pow(2, SMALLEST_LEVEL);

const int LEVEL_COUNT = BIGGEST_LEVEL - SMALLEST_LEVEL;

static const int BITMAP_ITEM_SIZE = 8;

const uint8_t OCCUPIED = 0;
const uint8_t AVAILABLE = 1;

class MetaData {

};

struct HeapBlock {
    HeapBlock *prev;
    HeapBlock *next;
    int bitmapIndex;

    HeapBlock() : prev(nullptr), next(nullptr), bitmapIndex(0) {}

    HeapBlock(HeapBlock *prev, HeapBlock *next, int bitmapIndex)
            : prev(prev), next(next), bitmapIndex(bitmapIndex) {}
};

class Heap {
private:
    void *memory;                                //Managed memory
    int totalSize;                               //Size of managed memory

    HeapBlock *availableBlocks[LEVEL_COUNT];     //Doubly linked list of all available blocks

    uint8_t *heapStartPointer;                   //Pointer to the start of the heap

    int dataSize;                                //Size of the heap that can be utilised to store data

    uint8_t *bitmap;                             //Array of bits representing occupation/availability
    int bitmapSize;                              //Size of the bitmap

    int levelOffset[LEVEL_COUNT];                //offset to the first byte representing the specific level

    int nextIndex;                               //Next block index in the bitmap
public:
    Heap() = default;

    Heap(void *memory, int size) : memory(reinterpret_cast<uint8_t *>(memory)),
                                   totalSize(size),
                                   bitmap(((uint8_t *) memory)),
                                   nextIndex(0) {
        // clear
        memset(memory, 0, totalSize);

        //estimate the number of all blocks
        cout << "Estimating count of blocks on all levels..." << endl;
        int levelsEstimate = levelCount(size);
        cout << "Blocks on all levels estimate: " << levelsEstimate << endl;

        //create bitmap - uint8_t is 8 bits, calculate the number of uint8_ts that will be filled out completely + dangling
        this->bitmapSize = levelsEstimate / BITMAP_ITEM_SIZE + (levelsEstimate % BITMAP_ITEM_SIZE != 0);
        cout << "Bitmap size " << bitmapSize << endl;
        setBitmap();

        //actual data size does not include
        this->dataSize = size - bitmapSize;
        heapStartPointer = static_cast<uint8_t *>(memory) + bitmapSize;

        cout << "Calculating precise count of blocks on all levels..." << endl;
        int levelsPrecise = levelCount(dataSize);

        //split data memory into chunks of powers of 2
        fragmentMemory();
    }

private:
    uint8_t *getPointer(int level, int positionInLevel) {

    }

    void fragmentMemory() {
        memset(availableBlocks, 0, LEVEL_COUNT * sizeof(HeapBlock *));

        //get a pointer offset by bitmap's size
        int remainingSize = dataSize;
        int offset = 0;
        while (remainingSize > SMALLEST_BLOCK_SIZE) {
            int blockSize = highestPowerOf2(remainingSize);
            int level = log2(blockSize);
            int index = levelToIndex(level);

            //create a block in memory
            auto block = reinterpret_cast<HeapBlock *>(heapStartPointer + offset);
            *block = HeapBlock(nullptr, nullptr, nextIndex);

            //Add to linked list at level
            HeapBlock **head = &availableBlocks[index];
            if (*head) {
                //There is already an existing block on the level

                //link
                (*head)->next = block;
                block->prev = (*head);
                *head = (*head)->next;
            } else {
                //There is no existing block, i.e. this will be the first
                *head = block;
            }

            //register as available in bitmap
            setBitAt(nextIndex);

            //update
            nextIndex++;
            offset += blockSize;
            remainingSize -= blockSize;
        }
    }

    void setBitmap() const {
        memset(bitmap, 0, bitmapSize * sizeof(uint8_t));
        bitmap[0] = 1;
    }

    int getBitAt(int index) {
        int byteIndex = index / BITMAP_ITEM_SIZE;
        int bitIndex = index % BITMAP_ITEM_SIZE;

        uint8_t byte = bitmap[byteIndex];
        int bit = (byte >> bitIndex) & 1;

        return bit;
    }

    void flipBitAt(int index) {
        int byteIndex = index / BITMAP_ITEM_SIZE;
        int bitIndex = index % BITMAP_ITEM_SIZE;

        bitmap[byteIndex] ^= (1 << bitIndex);
    }

    void setBitAt(int index) {
        int byteIndex = index / BITMAP_ITEM_SIZE;
        int bitIndex = index % BITMAP_ITEM_SIZE;

        bitmap[byteIndex] |= (1 << bitIndex);
    }

    static int levelToIndex(int level) {
        return level - SMALLEST_LEVEL;
    }

    static int indexToLevel(int index) {
        return index + SMALLEST_LEVEL;
    }

    static int levelCount(int availableSize) {
        //calculates how many blocks can be on all levels altogether, i.e. 1 on the highest, 2 on the one below and so on
        int levelCounter = 0;
        for (int i = BIGGEST_LEVEL; i >= SMALLEST_LEVEL; --i) {
            //calculates how many powers of 2 to i can be fit within size
            int level = pow(2, i);
            auto blocksOnLevel = availableSize / level;
            levelCounter += blocksOnLevel;

            if (blocksOnLevel)
                cout << "Level " << level << ": " << blocksOnLevel << endl;
        }

        return levelCounter;
    }

    static int highestPowerOf2(int n) {
        int exponent = log2(n);
        return pow(2, exponent);
    }
};

Heap heap;

void HeapInit(void *memPool, int memSize) {
    heap = Heap(memPool, memSize);
}

void *HeapAlloc(int size) {
    /* todo */

    // try to fragment to the smallest possible

    //stop if less than 4
}

bool HeapFree(void *blk) {
    /* todo */
}

void HeapDone(int *pendingBlk) {
    /* todo */
}


#ifndef __PROGTEST__

int main() {
    //static uint8_t memPool[3 * 1048576];
    static uint8_t memPool[2048];
    HeapInit(memPool, 1024);
//    AvailableBlock availableBlock = heap.getBlockAt();
    //cout << availableBlock.blockOffset;

    /*uint8_t *p0, *p1, *p2, *p3, *p4;
    int pendingBlk;
    static uint8_t memPool[3 * 1048576];


    HeapInit(memPool, 2097152);
    assert ((p0 = (uint8_t *) HeapAlloc(512000)) != NULL);
    memset(p0, 0, 512000);
    assert ((p1 = (uint8_t *) HeapAlloc(511000)) != NULL);
    memset(p1, 0, 511000);
    assert ((p2 = (uint8_t *) HeapAlloc(26000)) != NULL);
    memset(p2, 0, 26000);
    HeapDone(&pendingBlk);
    assert (pendingBlk == 3);


    HeapInit(memPool, 2097152);
    assert ((p0 = (uint8_t *) HeapAlloc(1000000)) != NULL);
    memset(p0, 0, 1000000);
    assert ((p1 = (uint8_t *) HeapAlloc(250000)) != NULL);
    memset(p1, 0, 250000);
    assert ((p2 = (uint8_t *) HeapAlloc(250000)) != NULL);
    memset(p2, 0, 250000);
    assert ((p3 = (uint8_t *) HeapAlloc(250000)) != NULL);
    memset(p3, 0, 250000);
    assert ((p4 = (uint8_t *) HeapAlloc(50000)) != NULL);
    memset(p4, 0, 50000);
    assert (HeapFree(p2));
    assert (HeapFree(p4));
    assert (HeapFree(p3));
    assert (HeapFree(p1));
    assert ((p1 = (uint8_t *) HeapAlloc(500000)) != NULL);
    memset(p1, 0, 500000);
    assert (HeapFree(p0));
    assert (HeapFree(p1));
    HeapDone(&pendingBlk);
    assert (pendingBlk == 0);


    HeapInit(memPool, 2359296);
    assert ((p0 = (uint8_t *) HeapAlloc(1000000)) != NULL);
    memset(p0, 0, 1000000);
    assert ((p1 = (uint8_t *) HeapAlloc(500000)) != NULL);
    memset(p1, 0, 500000);
    assert ((p2 = (uint8_t *) HeapAlloc(500000)) != NULL);
    memset(p2, 0, 500000);
    assert ((p3 = (uint8_t *) HeapAlloc(500000)) == NULL);
    assert (HeapFree(p2));
    assert ((p2 = (uint8_t *) HeapAlloc(300000)) != NULL);
    memset(p2, 0, 300000);
    assert (HeapFree(p0));
    assert (HeapFree(p1));
    HeapDone(&pendingBlk);
    assert (pendingBlk == 1);


    HeapInit(memPool, 2359296);
    assert ((p0 = (uint8_t *) HeapAlloc(1000000)) != NULL);
    memset(p0, 0, 1000000);
    assert (!HeapFree(p0 + 1000));
    HeapDone(&pendingBlk);
    assert (pendingBlk == 1);*/


    return 0;
}

#endif /* __PROGTEST__ */

