#ifndef __PROGTEST__

#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <climits>
#include <cfloat>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <utility>
#include <vector>
#include <set>
#include <list>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <queue>
#include <stack>
#include <deque>
#include <memory>
#include <functional>
#include <thread>
#include <mutex>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include "progtest_solver.h"
#include "sample_tester.h"

using namespace std;
#endif /* __PROGTEST__ */

const int MESSAGE_ID_MASK = 37;

uint64_t calculateID(uint64_t fragment) {
    return static_cast<uint32_t> (fragment >> MESSAGE_ID_MASK);
}

class Fragment {
public:
    const bool isClosing;
    const uint64_t fragment;

    Fragment(const bool isClosing, const uint64_t fragment) : isClosing(isClosing), fragment(fragment) {}
};

class Answer {
public:
    const bool isClosing;
    const bool isComplete;
    const CBigInt content;
    const uint64_t ID;

    Answer(const bool isClosing, const bool isComplete, const CBigInt &content, const uint64_t ID)
            : isClosing(isClosing), isComplete(isComplete), content(content), ID(ID) {}
};

class CSentinelHacker {
private:
    //Guff
    static CBigInt messageCounter;

    //Transmitters & receivers
    vector<ATransmitter> transmitters;
    vector<AReceiver> receivers;

    //Threads
    vector<thread> receiversThreads;
    vector<thread> transmitterThreads;
    vector<thread> workingThreads;

    //Data
    queue<Fragment> fragmentJobQueue;
    map<uint64_t, vector<uint64_t>> incompleteMessages;
    queue<Answer> answerJobQueue;

    //Mutexes
    mutex jobQueueMutex;
    mutex fragmentsByIDMutex;
    mutex answerJobMutex;

    //Condition variables for jobs
    condition_variable sendingJobIndicator;
    condition_variable workingJobIndicator;

    //------------------------------------------------------------------------------------------------------------------
    static CBigInt CountExpressionsWrapper(const uint8_t *data, size_t bits) {
        data += 4 /* Offset 4 bytes */;
        messageCounter = CountExpressions(data, bits - 32);
        return messageCounter;
    }

    void receive(const AReceiver &receiver) {
        uint64_t fragment;

        //get all the fragments
        while ((*receiver).Recv(fragment))
            addWorkJob({false, fragment});
    }

    //called outside of stop
    void transmit(const ATransmitter &transmitter) {
        while (true) {
            unique_lock lock(answerJobMutex);
            sendingJobIndicator.wait(lock,
                                     [this] { return !answerJobQueue.empty(); });
            auto job = answerJobQueue.front();
            answerJobQueue.pop();

            if (job.isClosing)
                return;

            if (job.isComplete) {
                transmitter->Send(job.ID, job.content);
            } else {
                transmitter->Incomplete(job.ID);
            }

        }

    }

    bool solve(const vector<uint64_t> &fragments, CBigInt &counter) {
        auto permutations = FindPermutations(fragments.data(), fragments.size(),
                                             [&counter](const uint8_t *data, size_t bits) {
                                                 data += 4;
                                                 counter = CountExpressions(data, bits - 32);
                                                 return counter;
                                             });

        return permutations != 0;
    }

    void work() {
        while (true) {
            unique_lock lock(jobQueueMutex);
            workingJobIndicator.wait(lock,
                                     [this] { return !fragmentJobQueue.empty(); });
            auto job = fragmentJobQueue.front();
            fragmentJobQueue.pop();

            //release lock
            lock.unlock();

            if (job.isClosing) //received a closing message
                return;
            uint64_t fragment = job.fragment;
            uint64_t ID = calculateID(fragment);

            auto fragments = getIncompleteMessageByID(ID);
            fragments.emplace_back(fragment);

            CBigInt result;

            if (!solve(fragments, result))
                addMessageFragment(ID, fragments);
            else {
                removeIncompleteMessageByID(ID);
                addAnswerJob({false, true, result, ID});
                sendingJobIndicator.notify_one();
            }

        }
    }

    void addWorkJob(const Fragment &fragment) {
        unique_lock lock(jobQueueMutex);
        fragmentJobQueue.push(fragment);
        lock.unlock();
        workingJobIndicator.notify_one();

    }

    void addWorkingThreadClosingJobs() {
        unique_lock lock(jobQueueMutex);

        for (uint i = 0; i < workingThreads.size(); ++i)
            fragmentJobQueue.push({true, 0});
        lock.unlock();

        workingJobIndicator.notify_all();
    }

    void addAnswerJob(const Answer &answer) {
        lock_guard lock(answerJobMutex);
        answerJobQueue.push(answer);
    }

    void addTransmittingThreadClosingJobs() {
        unique_lock lock(answerJobMutex);

        for (uint64_t i = 0; i < transmitterThreads.size(); ++i)
            answerJobQueue.push({true, false, 0, 0});
        lock.unlock();

        sendingJobIndicator.notify_all();
    }

    void addMessageFragment(uint64_t key, vector<uint64_t> &value) {
        lock_guard lock(fragmentsByIDMutex);
        incompleteMessages[key] = std::move(value);
    }

    void removeIncompleteMessageByID(uint64_t ID) {
        lock_guard lock(fragmentsByIDMutex);
        incompleteMessages.erase(ID);
    }

    vector<uint64_t> getIncompleteMessageByID(uint64_t ID) {
        lock_guard lock(fragmentsByIDMutex);
        return incompleteMessages[ID];
    }

public:
    static bool SeqSolve(const vector<uint64_t> &fragments, CBigInt &res) {
        auto permutations = FindPermutations(fragments.data(), fragments.size(), CountExpressionsWrapper);
        res = messageCounter;
        messageCounter = 0;
        return permutations != 0;
    }

    void AddTransmitter(ATransmitter x) {
        transmitters.emplace_back(x);
    }

    void AddReceiver(AReceiver x) {
        receivers.emplace_back(x);
    }

    void AddFragment(uint64_t x) {
        addWorkJob({false, x});
    }

    void Start(unsigned thrCount) {
        //start receivers
        for (const auto &receiver: receivers)
            receiversThreads.emplace_back(thread(&CSentinelHacker::receive, this, cref(receiver)));

        //start working threads
        for (unsigned i = 0; i < thrCount; ++i)
            workingThreads.emplace_back(thread(&CSentinelHacker::work, this));
        //start transmitters
        for (const auto &transmitter: transmitters)
            transmitterThreads.emplace_back(thread(&CSentinelHacker::transmit, this, cref(transmitter)));
    }

    void addIncompleteAnswers() {
        lock_guard lock(answerJobMutex);
        for (const auto &key: incompleteMessages)
            answerJobQueue.push({false, false, 0, key.first});
    }

    void Stop() {
        for (auto &receiver : receiversThreads)
            receiver.join();

        addWorkingThreadClosingJobs();

        for (auto &worker: workingThreads)
            worker.join();

        addIncompleteAnswers();
        addTransmittingThreadClosingJobs();

        for (auto &transmitter: transmitterThreads)
            transmitter.join();
    }

};

//initialise static counter
CBigInt CSentinelHacker::messageCounter;
// TODO: CSentinelHacker implementation goes here
//-------------------------------------------------------------------------------------------------
#ifndef __PROGTEST__

int main() {
    using namespace std::placeholders;
    for (const auto &x : g_TestSets) {
        CBigInt res;
        assert (CSentinelHacker::SeqSolve(x.m_Fragments, res));
        assert (CBigInt(x.m_Result).CompareTo(res) == 0);
    }

    CSentinelHacker test;
    auto trans = make_shared<CExampleTransmitter>();
    AReceiver recv = make_shared<CExampleReceiver>(
            initializer_list<uint64_t>{0x02230000000c, 0x071e124dabef, 0x02360037680e, 0x071d2f8fe0a1, 0x055500150755});

    test.AddTransmitter(trans);
    test.AddReceiver(recv);
    test.Start(3);

    static initializer_list<uint64_t> t1Data = {0x071f6b8342ab, 0x0738011f538d, 0x0732000129c3, 0x055e6ecfa0f9,
                                                0x02ffaa027451, 0x02280000010b, 0x02fb0b88bc3e};
    thread t1(FragmentSender, bind(&CSentinelHacker::AddFragment, &test, _1), t1Data);

    static initializer_list<uint64_t> t2Data = {0x073700609bbd, 0x055901d61e7b, 0x022a0000032b, 0x016f0000edfb};
    thread t2(FragmentSender, bind(&CSentinelHacker::AddFragment, &test, _1), t2Data);
    FragmentSender(bind(&CSentinelHacker::AddFragment, &test, _1),
                   initializer_list<uint64_t>{0x017f4cb42a68, 0x02260000000d, 0x072500000025});
    t1.join();
    t2.join();
    test.Stop();
    assert (trans->TotalSent() == 4);
    assert (trans->TotalIncomplete() == 2);
    return 0;
}

#endif /* __PROGTEST__ */
